package piengine.object.model.service;

import piengine.object.model.domain.Model;
import piengine.object.model.shader.SolidModelShader;
import piengine.visual.render.domain.RenderContext;
import piengine.visual.render.domain.RenderType;
import piengine.visual.render.interpreter.RenderInterpreter;
import piengine.visual.render.service.AbstractRenderService;
import piengine.visual.shader.service.ShaderService;
import piengine.visual.texture.service.TextureService;
import puppeteer.annotation.premade.Component;
import puppeteer.annotation.premade.Wire;

import static org.lwjgl.opengl.GL11.GL_BACK;
import static piengine.visual.render.domain.RenderType.RENDER_SOLID_MODEL;

@Component
public class SolidModelRenderService extends AbstractRenderService<SolidModelShader> {

    private final TextureService textureService;
    private final RenderInterpreter renderInterpreter;

    @Wire
    public SolidModelRenderService(final ShaderService shaderService,
                                   final TextureService textureService,
                                   final RenderInterpreter renderInterpreter) {
        super(shaderService);

        this.textureService = textureService;
        this.renderInterpreter = renderInterpreter;
    }

    @Override
    protected SolidModelShader createShader(final ShaderService shaderService) {
        return shaderService.supply("solidModelShader").castTo(SolidModelShader.class);
    }

    @Override
    public void render(final RenderContext context) {
        renderInterpreter.setCullFace(GL_BACK);
        renderInterpreter.setDepthTest(true);

        shader.start()
                .loadLight(context.light)
                .loadProjectionMatrix(context.camera.projection)
                .loadViewMatrix(context.camera.view);
        textureService.bind(context.texture);
        for (Model model : context.models) {
            shader.loadModelMatrix(model.getModelMatrix());
            renderInterpreter.render(model.mesh.dao);
        }
        shader.stop();
    }

    @Override
    public RenderType getType() {
        return RENDER_SOLID_MODEL;
    }

}
