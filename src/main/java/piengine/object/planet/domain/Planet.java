package piengine.object.planet.domain;

import piengine.object.entity.domain.Entity;
import piengine.object.mesh.domain.Mesh;
import piengine.object.model.domain.Model;

public class Planet extends Model {

    public Planet(final Mesh mesh, final Entity parent) {
        super(mesh, parent);
    }

}
