package piengine.core.base.api;

public interface Updatable {
    void update(double delta);
}
