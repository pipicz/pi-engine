package piengine.visual.texture.domain;

import piengine.core.base.domain.Dao;

public class TextureDao implements Dao {

    public final int id;

    public TextureDao(final int id) {
        this.id = id;
    }

}
