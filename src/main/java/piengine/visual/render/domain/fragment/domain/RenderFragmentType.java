package piengine.visual.render.domain.fragment.domain;

public enum RenderFragmentType {
    SET_VIEW_PORT,
    SET_CLEAR_COLOR,
    SET_MODELS,
    SET_TEXTURE,
    SET_CAMERA,
    SET_LIGHT,
    SET_TEXT,
    SET_PLANET,
    SET_ASSET,
    DO_CLEAR_SCREEN,
    DO_RENDER
}
