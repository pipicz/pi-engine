package piengine.visual.render.domain;

public enum RenderType {
    RENDER_PLANE_MODEL,
    RENDER_SOLID_MODEL,
    RENDER_TEXT,
    RENDER_PLANET
}
