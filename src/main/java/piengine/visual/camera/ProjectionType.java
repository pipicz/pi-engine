package piengine.visual.camera;

public enum ProjectionType {
    ORTHOGRAPHIC,
    PERSPECTIVE
}