package piengine.visual.window.domain;

public enum WindowEventType {
    INITIALIZE,
    UPDATE,
    CLOSE
}
